//
//  ffmpeg.swift
//  FFmpegExp
//
//  Created by jayios on 2016. 8. 4..
//  Copyright © 2016년 gretech. All rights reserved.
//

import Foundation
import ffmpeg

func tutorial1(path: String) -> Bool{
    
    var pFormatCtx = avformat_alloc_context()
    
    guard 0 == is_err(avformat_open_input(&pFormatCtx, path.cStringUsingEncoding(NSUTF8StringEncoding)!, nil, nil)) else {
        return false
    }
    
    defer {
        avformat_close_input(&pFormatCtx)
        avformat_free_context(pFormatCtx)
        print("closed -> \(path)")
    }
    
    guard 0 == is_err(avformat_find_stream_info(pFormatCtx, nil)) else {
        return false
    }
    
    av_dump_format(pFormatCtx, 0, path.cStringUsingEncoding(NSUTF8StringEncoding)!, 0)
    
    var i: Int32 = 0
    var videoStream: Int = -1
    for si in 0..<pFormatCtx.memory.nb_streams {
        let stream = pFormatCtx.memory.streams.advancedBy(Int(si))
        switch stream.memory.memory.codecpar.memory.codec_type {
        case AVMEDIA_TYPE_VIDEO:
            videoStream = Int(si)
        default:
            break
        }
    }
    
    if -1 == videoStream {
        return false
    }
    
    var pCodecCtx = pFormatCtx.memory.streams.advancedBy(videoStream).memory.memory.codec
    
    var pCodec = avcodec_find_decoder(pCodecCtx.memory.codec_id)
    
    if nil == pCodec {
        return false
    }
    
    defer {
        avcodec_close(pCodecCtx)
    }
    
    guard 0 == is_err(avcodec_open2(pCodecCtx, pCodec, nil)) else {
        return false
    }
    
    let pFrame = av_frame_alloc()
    let pFrameRGB = av_frame_alloc()
    
    if nil == pFrameRGB {
        return false
    }
    
    
    let numBytes = av_image_get_buffer_size(AV_PIX_FMT_RGB24, pCodecCtx.memory.width, pCodecCtx.memory.height, 0)
    
    let buffer = UnsafeMutablePointer<UInt8>(av_malloc(Int(numBytes) * sizeof(UInt8)))
    
    avpicture_fill(UnsafeMutablePointer<AVPicture>(pFrameRGB), buffer, AV_PIX_FMT_RGB24, pCodecCtx.memory.width, pCodecCtx.memory.height)
    
    let graph = avfilter_graph_alloc()
    let buffersrc = avfilter_get_by_name("buffer")
    let buffersink = avfilter_get_by_name("buffersink")
    let outputs = avfilter_inout_alloc()
    let inputs = avfilter_inout_alloc()
    let pix_fmt = AV_PIX_FMT_RGB24
    
    
    avfilter_graph_create_filter(<#T##filt_ctx: UnsafeMutablePointer<UnsafeMutablePointer<AVFilterContext>>##UnsafeMutablePointer<UnsafeMutablePointer<AVFilterContext>>#>, <#T##filt: UnsafePointer<AVFilter>##UnsafePointer<AVFilter>#>, <#T##name: UnsafePointer<Int8>##UnsafePointer<Int8>#>, <#T##args: UnsafePointer<Int8>##UnsafePointer<Int8>#>, <#T##opaque: UnsafeMutablePointer<Void>##UnsafeMutablePointer<Void>#>, <#T##graph_ctx: UnsafeMutablePointer<AVFilterGraph>##UnsafeMutablePointer<AVFilterGraph>#>)
    
    return true
}