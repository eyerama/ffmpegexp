//
//  SGShareDocuments.swift
//  SlothGom
//
//  Created by jayios on 2016. 6. 23..
//  Copyright © 2016년 Gretech. All rights reserved.
//

import Foundation

protocol SystemPathable {
    var path: String {get}
    func add(pathComponent: String) -> String
}

protocol PathAttributeType {
    var attributes: [String : AnyObject]? {get}
    var destinationOfSymbolicLinkAtPath: String? { get }
    var isFilePath: Bool {get}
}

extension String: SystemPathable, PathAttributeType {}

extension NSSearchPathDirectory: SystemPathable {}

extension PathAttributeType where Self: SystemPathable {
    
    var attributes: [String: AnyObject]? {
        do {
            return try NSFileManager.defaultManager().attributesOfItemAtPath(self.path)
        }
        catch let err as NSError {
            print(err)
        }
        return nil
    }
    
    var destinationOfSymbolicLinkAtPath: String? {
        return try? NSFileManager.defaultManager().destinationOfSymbolicLinkAtPath(self.path)
    }
    
    var isFilePath: Bool {
        
        guard let attribute = self.attributes else {
            return false
        }
        let type = attribute[NSFileType] as! String
        switch type {
        case NSFileTypeRegular:
            return true
        default:
            return false
        }
    }
}

extension SystemPathable {
    var path: String {
        switch self {
        case let directory as NSSearchPathDirectory:
            return NSSearchPathForDirectoriesInDomains(directory, .UserDomainMask, true)[0]
        case let string as String:
            return string
        default:
            return ""
        }
    }
    func add(pathComponent: String) -> String {
        let pathString = self.path as NSString
        return pathString.stringByAppendingPathComponent(pathComponent)
    }
}

@objc public class ShareDocuments: NSObject {
    public static func share() {
        #if arch(i386) || arch(x86_64)
            let documentPath = NSSearchPathDirectory.DocumentDirectory.path
            let documents = documentPath.componentsSeparatedByString("/")
            let symbolicDocumentPath = documents[1].add(documents[2]).add("Documents").add(BundleName.valueForMainBundle)
            
            defer {
                do {
                    try NSFileManager.defaultManager().createSymbolicLinkAtPath(symbolicDocumentPath, withDestinationPath: documentPath)
                } catch let err as NSError {
                    print(err)
                }
            }
            
            guard let _ = symbolicDocumentPath.attributes else {
                return
            }
            
            if documentPath != symbolicDocumentPath.destinationOfSymbolicLinkAtPath {
                do {
                    try NSFileManager.defaultManager().removeItemAtPath(symbolicDocumentPath)
                } catch let err as NSError {
                    print(err)
                }
            }
        #endif
    }
}