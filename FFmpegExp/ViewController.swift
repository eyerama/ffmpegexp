//
//  ViewController.swift
//  FFmpegExp
//
//  Created by jayios on 2016. 8. 4..
//  Copyright © 2016년 gretech. All rights reserved.
//

import UIKit


class ViewController: UICollectionViewController {
    
    
    let path = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
    var documentsObserver: dispatch_source_t!
    var fd: CInt!
    var documents: [String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        fd = open(path.cStringUsingEncoding(NSUTF8StringEncoding)!, O_RDONLY)
        self.documentsObserver = dispatch_source_create(DISPATCH_SOURCE_TYPE_VNODE, UInt(fd), DISPATCH_VNODE_WRITE | DISPATCH_VNODE_DELETE, dispatch_get_main_queue())
        dispatch_source_set_event_handler(self.documentsObserver) {
            print("reloaded")
            self.reload()
        }
        dispatch_source_set_cancel_handler(self.documentsObserver) { 
            close(self.fd)
        }
        
        let layout = UICollectionViewFlowLayout()
        self.collectionView?.collectionViewLayout = layout
        layout.estimatedItemSize = CGSize(width: self.collectionView?.bounds.width ?? 320, height: 40)
        
        self.reload()
        dispatch_resume(self.documentsObserver)
    }
    
    deinit {
        dispatch_source_cancel(self.documentsObserver)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reload() {
        self.documents = try? NSFileManager.defaultManager().contentsOfDirectoryAtPath(self.path).lazy.filter(){!$0.hasPrefix(".")} ?? []
        self.collectionView?.reloadData()
    }
    
    func file(fromIndexPath indexPath: NSIndexPath) -> String? {
        guard let file = self.documents?[indexPath.row] else {
            return nil
        }
        return self.path + "/\(file)"
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.documents?.count ?? 0
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! Cell
        
        let file = self.documents![indexPath.row]
        
        cell.titleLabel.text = file
        
        return cell
    }
    
}

//MARK: Selections
extension ViewController {
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        guard let path = self.file(fromIndexPath: indexPath) else {
            return
        }
        guard let cell = collectionView.cellForItemAtIndexPath(indexPath) as? Cell else {
            return
        }
        UIView.animateWithDuration(0.25, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: [.CurveEaseInOut, .BeginFromCurrentState], animations: {
                cell.titleLabel.transform = CGAffineTransformMakeScale(1.25, 1.25)
        }){
            if $0 {
                UIView.animateWithDuration(0.25, animations: { 
                    cell.titleLabel.transform = CGAffineTransformIdentity
                })
            }
        }
        if tutorial1(path) {
            print("succeed")
        } else {
            print("failed")
        }
    }
}