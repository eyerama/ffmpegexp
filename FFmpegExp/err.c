//
//  err.c
//  FFmpegExp
//
//  Created by jayios on 2016. 8. 4..
//  Copyright © 2016년 gretech. All rights reserved.
//

#include "err.h"
#import <libavformat/avformat.h>

int is_err(int err) {
    if (0 == err) {
        return 0;
    }
    
    printf("%s\n", av_err2str(err));
    
    return 1;
}