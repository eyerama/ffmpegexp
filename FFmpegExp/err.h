//
//  err.h
//  FFmpegExp
//
//  Created by jayios on 2016. 8. 4..
//  Copyright © 2016년 gretech. All rights reserved.
//

#ifndef err_h
#define err_h

#include <stdio.h>

int is_err(int err);

#endif /* err_h */
